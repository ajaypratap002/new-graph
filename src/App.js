import React, { Component } from 'react';
import { Redirect, Route, Router } from 'react-router-dom';
import { connect } from 'react-redux';
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import history from './Data/history';

//Import Navigation Components.
import { Sidebar, TopNav } from './Navigation';

//Import Custom Components.
import AddBook from './Views/Login/login.js';
import Home from './Views/Home/Home';
import Users from './Views/Users/users.js';
import Validate from './Security/Validate/validate.js';

import './Styling/App.css';

const client = new ApolloClient({
  uri: "http://192.168.2.68:3000/graphql"
});

class App extends Component {

  constructor(props) {

    super(props);

    this.state = { type: 'editor' };
  }


  logout() {
    // this.props.auth.logout();
  }

  render() {

    return (
      <ApolloProvider client={client}>
        <AddBook />
      </ApolloProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userAuth: state.main.userAuth
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUserType: (type) => {
      dispatch(
        {
          type: "UpdateUserType",
          payload: type
        }
      );
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);