import ReactDOM from 'react-dom';
import {makeMainRoutes} from './Routing/routes';

const routes = makeMainRoutes();

ReactDOM.render(
  routes,
  document.getElementById('root')
);
