import React from 'react';
import { Redirect, Route, Router } from 'react-router-dom';

//Main App Root
import App from '../App';

//Browser history
import history from '../Data/history';

/* Import Redux Elements */
import store from '../Data/Store/store.js';
import { Provider } from 'react-redux';

//Auth0 Main Component and Auth Views
import Login from '../Views/Login/login.js';
import Callback from '../Views/Callback/Callback';


export const makeMainRoutes = () => {
  return (
    <div>
      <Provider store={store}>
        <Router history={history} component={App}>
            <div>
                
                {/* Main App View Route */}
                <Route path="/dashboard"  render={(props) => <App  />} />
                
                {/* Redirect Root Requests to Login Page */}
                <Route path="/" exact render={() => <Redirect to="/dashboard/login"/>} />
                
                {/* Auth0 Views */}
                <Route path="/login" render={(props) => <Login  />} />

                <Route path="/callback" render={(props) => {
            
                  return <Callback {...props} />;
                }}/>

            </div>
         </Router>
       </Provider>
     </div>
  );
};
