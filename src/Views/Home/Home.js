import React, { Component, Suspense } from 'react';
import { connect } from 'react-redux';

//Import plugins
const jQuery = require("jquery");

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      version: 'none',
    };

  }

  componentDidMount() {

    jQuery('.main-panel').show();
    jQuery('.sidebar').show();

    //Set Page Name
    this.props.changeInitial('Home');

    this.getUser();

  }

  componentWillMount() {

  }

  getUser = () => {

  }

  render() {

    return (

      <div>

        {this.state.version == 'none' && (
          <div className="container-fluid">

          </div>
        )}

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    main: state,
    initial: state.main.initial,
    userAuth: state.main.userAuth,
    usersAPI: state.users.usersAPI
  };
};

const mapDispatchToProps = (dispatch) => {
  return {

    changeInitial: (initial) => {
      dispatch(
        {
          type: "ChangeInitial",
          payload: initial
        }
      );
    },
  

  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
