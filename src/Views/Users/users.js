import React, { Component } from 'react';
import { connect } from 'react-redux';

//Import Components
import UsersTblPage from './queries/table.js';

//import UI
import CardWithHeader from '../../UI/cardWithHeader.js';

import "./styles/styles.css";

//Import Plugins
const axios = require('axios');

class Users extends Component {

    constructor(props) {
        super(props);

        this.state = {
            initial: '',
            users: [],
            isLoading: true,
            isUserDetailDisplay: false,
        };

    }

    /* Life Cycle Events */
    componentWillMount() {

        this.props.changeUserAction({ type: "NONE", userId: "" });

        this.props.changeInitial('Users');

      
    }

    componentDidMount() {

        this.getAllUsers();
    }


    //Find all vanities for the current 
    getAllUsers = () => {

        this.setState({ isLoading: true });
        this.setState({ users: [] });
        this.setState({ isUserDetailDisplay: false });

        let getURL = this.props.usersAPI + 'api/users/';

        axios.get(getURL).then((response) => {

            if (response.status === 200) {

                this.setState({ users: response.data });
                this.setState({ isLoading: false });
                this.props.changeUserAction({ type: "NONE", userId: "" });

            }

        }).catch((error) => {

            console.log(error);
            window.swal({ text: 'Could not load users. Please try again.' });

        });

    }

    render() {

        return (
            <div>

                {!this.state.isUserDetailDisplay &&

                    <CardWithHeader title="Users" icon="people" color="red">

                        <br />

                        <button id="refreshAllUser" onClick={this.getAllUsers.bind(this)} className="btn btn-info btn-fill pull-right" data-type="save">
                            <span className="btn-label">
                                <i data-type="save" className="material-icons">refresh</i>
                            </span> Refresh List
                        </button>

                        <div style={{ "clear": "both" }}></div>

                        <UsersTblPage data={this.state.users} />


                        {this.state.isLoading &&
                            < img style={{ margin: '0 auto', height: '50px', width: '50px' }} src="" className="img-responsive" alt="Not Found!" />

                        }
                        {!this.state.isLoading && this.state.users.length <= 0 &&
                            <div style={{ textAlign: 'center', marginTop: '10px' }}>No User Found</div>

                        }

                    </CardWithHeader>
                }

            </div >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        initial: state.main.initial,
        usersAPI: state.users.usersAPI,
        userAction: state.users.userAction,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

        changeInitial: (initial) => {
            dispatch(
                {
                    type: "ChangeInitial",
                    payload: initial
                }
            );
        },
        changeUserAction: (userAction) => {
            dispatch(
                {
                    type: "changeUserAction",
                    payload: userAction
                }
            );
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
