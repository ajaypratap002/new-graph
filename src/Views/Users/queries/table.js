import React from 'react';
import SortableTbl from 'react-sort-search-table';

import Manager from './manager.js';

const ProductsTblPage = (props) => {

    let col = [
        "email",
        "username",
        "is_staff",
    ];
    let tHead = [
        "Email",
        "Name",
        "Role Type"
    ];


    return (
        <SortableTbl tblData={props.data}
            tHead={tHead}
            dKey={col}
            customTd={[
                { custd: Manager, keyItem: "manage" },
            ]}
            defaultCSS={true}
        />
    );
};


export default (ProductsTblPage);
