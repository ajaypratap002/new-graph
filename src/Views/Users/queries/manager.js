import React from 'react';
import { connect } from 'react-redux';
import { Modal, Button } from 'antd';

//Plugins
const axios = require('axios');
const jQuery = require("jquery");
const confirm = Modal.confirm;

class Manager extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			fullData: [],
			headerParams: {
				headers: {
					'Authorization': localStorage.getItem('api_access_token'),
					'Email': localStorage.getItem('email'),

				}
			}
		};

	}

	componentDidMount() {
		//Fix package spelling
		jQuery('.sortable-table .desc').hide();

	}

	deleteItem(id) {

		let url = this.props.usersAPI + 'delete/';
		let headerParams = this.state.headerParams;

		confirm({
			title: 'Are you sure you want to delete this User?',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk() {

				axios.post(url, { 'id': id }, headerParams)

					.then((response) => {

						if (response.data.status === "success") {
							document.getElementById('refreshAllUser').click();
							Modal.success({ title: "Success", content: "User deleted successfully." });
						}



					})
					.catch((error) => {

						Modal.error({ title: "Error", content: 'Sorry, we could not delete the user. Please try again.' });

					});

			}
		});


	}

	view = () => {
		this.props.changeUserAction({ type: "View", userId: this.props.rowData.id });
	}

	edit = () => {

		this.props.changeUserAction({ type: "Edit", userId: this.props.rowData.id });
	}

	render() {
		return (
			<td className="actionsMenu user-action-button">

			
				<button id="EditUser" onClick={this.edit} title="Edit User" className="btn btn-xs btn-success btn-fill" ><span className="btn-label">Edit</span></button>

				<button id="delete" title="Delete" onClick={this.deleteItem.bind(this, this.props.rowData.user_id)} className="btn btn-xs btn-danger btn-fill" ><span className="btn-label">Delete</span></button>


			</td>

		);
	}
}


const mapStateToProps = (state) => {
	return {
		api: state.main.api,
		client: state.main.client,
		usersAPI: state.users.usersAPI,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {

		changeUserAction: (userAction) => {
			dispatch(
				{
					type: "changeUserAction",
					payload: userAction
				}
			);
		}


	};
};


export default connect(mapStateToProps, mapDispatchToProps)(Manager);
