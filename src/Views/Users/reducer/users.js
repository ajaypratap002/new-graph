let live = "http://192.168.2.85:8585/";

if (window.location.hostname === "localhost" || window.location.hostname === "192.168.2.85") {

    live = 'http://192.168.2.85:8585/';

}

const initialState = {
    usersAPI: live,
    userAuth: {},
    userAction: {
        type: "NONE",
        userId: "NONE",
    },
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case "changeUserAction":
            state = {
                ...state,
                userAction: action.payload,
            };
            break;
     
    }
    return state;
};

export default userReducer;
