import React, { Component } from "react";
import { graphql } from "react-apollo";
import {flowRight as compose} from 'lodash';

import gql from 'graphql-tag';

const addBookMutation = gql`
  mutation {
    createUser(data: { name: "", email: "" })
    {
        id
    }
  }
`;

class AddBook extends Component {
  state = {
    name: "",
    email: "",
    authorId: ""
  };

  submitForm(e) {
    e.preventDefault();
    console.log(this.state);
  }

  render() {
    return (
      <form onSubmit={this.submitForm.bind(this)}>
        <div className="field">
          <label>Book name: </label>
          <input
            type="text"
            onChange={e => {
              this.setState({ name: e.target.value });
            }}
          />
        </div>
        <div className="field">
          <label>Genre: </label>
          <input
            type="text"
            onChange={e => {
              this.setState({ email: e.target.value });
            }}
          />
        </div>
        <div className="field">
          <label>Author: </label>
        </div>
        <button>+</button>
      </form>
    );
  }
}

export default compose(
  graphql(addBookMutation, { name: "addBookMutation" })
)(AddBook);