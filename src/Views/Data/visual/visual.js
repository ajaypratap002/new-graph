import React, { Component } from 'react';
import {connect} from 'react-redux';

//Import Components
import {ClientChoose} from '../../Clients';
import Dropzone from 'react-dropzone';

//import UI
import CardWithHeader from '../../UI/cardWithHeader.js';
import { SideSheet, Heading, Pane, Button, Card } from 'evergreen-ui';

//Import mock data
import columns from '../../../Data/Mock/table_sql_columns.js';


//Import Plugins
const jQuery = require("jquery");
const axios = require('axios');

let uploaderStyle = {
    width: '100%',
    clear: 'both',
    padding: '20px'
};


class Visual extends Component {

  constructor(props){
        super(props);

        this.state = {

            initial: '',
            isShown: false,
            sidebar: 'elements',
            sidebarHeight: 310,
            sidebarTitle: 'None'

        };

  }
  
  /* Life Cycle Events */
  componentWillMount(){
      
      this.setState({builder: 1});
      if(this.props.client !== "NONE"){
        
      }
      
  }

  componentDidMount(){
      
      this.renderRules();
      
  }
  

  
  componentWillReceiveProps(){
      setTimeout(()=>{
          
          if(this.props.client !== "NONE"){
              
          }
          
      },300);
  }
  
  showQueryElements = ()=>{
     this.setState({ isShown: true });
     this.setState({ sidebar: 'uploader' });
     this.setState({ sidebarHeight: 310 });
     this.setState({ sidebarTitle: 'Query Elements' });
 }
 
 updateRules = ()=>{
     
     this.setState({builder: this.state.builder + 1});
     setTimeout(()=>{
         this.renderRules();
     }, 200);
     
 }
 
 renderRules = ()=>{
     
     const rules = [];

     for (var i = 0; i < this.state.builder; i += 1) {
      rules.push(this.addRule(i));
     }
    
     this.setState({rules: rules});
     
 }
 
 deleteRule = ()=>{
     
     this.setState({builder: this.state.builder - 1});
     setTimeout(()=>{
         this.renderRules();
     }, 200);
     
 }
 
 addRule = (key)=>{
     
     return(
       <div key={key} className="row ruleRow">
       
          <div className="col-sm-2">
            Type of query:
            <select>
                <option value="SELECT">SELECT ALL</option>
                <option value="COUNT">COUNT</option>
                <option value="MATH">MATH</option>
            </select>
          </div>
          
          <div className="col-sm-2">
            Which Columns: &nbsp;
            <select>
                <option value="none">All</option>
                <option value="Amount">Amount</option>
            </select>
          </div>
          
          
          <div className="col-sm-2">
            Where
            <select>
                <option value="none">All</option>
                <option value="Amount">Amount</option>
            </select>
          </div>
          
          <div className="col-sm-2">
            Condition
            <select>
                <option value="=">Equals</option>
                <option value="!=">Does Not Equal</option>
                <option value="!=">Contains</option>
                <option value="!=">Does Not Contain</option>
            </select>
          </div>
          
          <div className="col-sm-1">
            Value
            <input type="text" />
          </div>
          
           <div className="col-sm-1">
            
            <button onClick={this.deleteRule}>Delete</button>
            
          </div>
          
          
       </div>    
      );
     
 }


  render() {


    return (
        
        <div style={uploaderStyle}>
            
            <p>Visual SQL Editor</p>
            
            <div id="">
            
                <div id="visual-sql-toolbar">
                
                      {/* 
                      <Button iconBefore="add" onClick={this.showQueryElements}>
                       Upload Files
                      </Button>
                      
                      &nbsp;
                      &nbsp;
                      
                      <Button iconBefore="search" onClick={this.showQueryElements}>
                       Manage Files
                      </Button>
                      */}
                      
                      <div className="row">
                        <div className="col-sm-12">
                            Which table are you querying? &nbsp;
                            <select>
                                <option value="none">Please Choose</option>
                                <option value="Transactions_Default">Transactions_Default</option>
                            </select>
                        </div>
                      </div>
                      
                      <br/>
                      
                      <div className="row">
                        <div className="col-sm-12">
                            
                            <Button iconBefore="add" onClick={this.updateRules}>New Rule</Button>
                            
                        </div>
                      </div>
                      
                      <br/>
                      
                      {this.state.rules}
         
                      
                      
                </div>
                
                <SideSheet
                    isShown={this.state.isShown}
                    onCloseComplete={() => this.setState({ isShown: false })}
                    containerProps={{
                      display: 'flex',
                      flex: '1',
                      flexDirection: 'column',
                    }}
                  >
                    <Pane flexShrink={0} elevation={0} backgroundColor="white">
                      <Pane padding={16}>
                        <Heading size={600}>{this.state.sidebarTitle}</Heading>
                      </Pane>
                    </Pane>
                    
                    <Pane flex="1" overflowY="scroll" appearance="tint1" padding={16}>
                      <Card
                        backgroundColor="white"
                        elevation={0}
                        height={310}
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                      >
                        
                        <p>Elements available to make queries</p>
                        
                      </Card>
                    </Pane>
                    
                </SideSheet>
                
            
            </div>
        
        </div>
    );
  }
}


const mapStateToProps = (state) => {
    return {
        client:  state.main.client,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Visual);


