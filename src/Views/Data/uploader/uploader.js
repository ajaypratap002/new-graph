import React, { Component } from 'react';
import {connect} from 'react-redux';

//Import Components
import Dropzone from 'react-dropzone';

//import UI
import CardWithHeader from '../../../UI/cardWithHeader.js';


//Import Plugins
const jQuery = require("jquery");
const axios = require('axios');

let uploaderStyle = {
    width: '100%',
    clear: 'both',
    padding: '20px'
};

class Uploader extends Component {

  constructor(props){
        super(props);

        this.state = {

            initial: '',

        };

  }
  
  /* Life Cycle Events */
  componentWillMount(){
      
      if(this.props.client !== "NONE"){
        
      }
      
  }

  componentDidMount(){}
  
  componentWillReceiveProps(){
      setTimeout(()=>{
          
          if(this.props.client !== "NONE"){
              
          }
          
      },300);
  }

  onDrop = (acceptedFiles, rejectedFiles)=>{

  }
 

  render() {


    return (
        
        <div style={uploaderStyle}>
        
             <br/>
             <p>1. Please upload only <strong>one file at a time</strong> in CSV format.</p>


             <br/>
             <div className="dropzone">
                  <Dropzone onDrop={this.onDrop}>
                    <p>Click here or drag and drop files for your project here.</p>
                  </Dropzone>
             </div>
        
        </div>
    );
  }
}


const mapStateToProps = (state) => {
    return {
        client:  state.main.client,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Uploader);


