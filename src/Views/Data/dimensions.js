import React, { Component } from 'react';
import {connect} from 'react-redux';

//Import Components
import {ClientChoose} from '../Clients';
import Uploader from './uploader/uploader.js';
import Visual from './visual/visual.js';

//import UI
import CardWithHeader from '../UI/cardWithHeader.js';
import { SideSheet, Heading, Pane, Button, Card } from 'evergreen-ui';


//Import Plugins
const jQuery = require("jquery");
const axios = require('axios');


class Dimensions extends Component {

  constructor(props){
        super(props);

        this.state = {

            initial: '',
            domain: '',
            vanities: [],
            loadedVanity: '',
            isShown: false,
            sidebar: 'uploader',
            sidebarHeight: 310,
            sidebarTitle: 'None'

        };

  }
  
  /* Life Cycle Events */
  componentWillMount(){
      
      if(this.props.client !== "NONE"){
        
      }
      
  }

  componentDidMount(){}
  
  componentWillReceiveProps(){
      setTimeout(()=>{
          
          if(this.props.client !== "NONE"){
              
          }
          
      },300);
  }

 
 showUploader = ()=>{
     this.setState({ isShown: true });
     this.setState({ sidebar: 'uploader' });
     this.setState({ sidebarHeight: 310 });
     this.setState({ sidebarTitle: 'Transaction Manager' });
 }
 

  render() {

    const { isAuthenticated } = this.props.auth;


    return (
        <div>

         
            <CardWithHeader title="Vanity URL" icon="arrow_right_alt" color="red">
                      
                  <br/>
                  
                  <div id="data-toolbar" className="pull-left">
                      <Button iconBefore="add" onClick={this.showUploader}>
                       Upload Files
                      </Button>
                      
                      &nbsp;
                      &nbsp;
                      
                      <Button iconBefore="search" onClick={this.showUploader}>
                       Manage Files
                      </Button>
                  </div>
                  
                  <br/>
                  <br/>
                  
                  <Visual />
                    
            
                 <SideSheet
                    isShown={this.state.isShown}
                    onCloseComplete={() => this.setState({ isShown: false })}
                    containerProps={{
                      display: 'flex',
                      flex: '1',
                      flexDirection: 'column',
                    }}
                  >
                    <Pane flexShrink={0} elevation={0} backgroundColor="white">
                      <Pane padding={16}>
                        <Heading size={600}>{this.state.sidebarTitle}</Heading>
                      </Pane>
                    </Pane>
                    
                    <Pane flex="1" overflowY="scroll" appearance="tint1" padding={16}>
                      <Card
                        backgroundColor="white"
                        elevation={0}
                        height={310}
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                      >
                         <Uploader />
                            
                         <br/>
                         
                      </Card>
                    </Pane>
                    
                </SideSheet>
                
            
            </CardWithHeader>
        
        </div>
    );
  }
}



const mapStateToProps = (state) => {
    return {
        initial : state.main.initial,
        role : state.main.role,
        api: state.main.api,
        client:  state.main.client,
        domain: state.main.domain,
        dataState: state.dataReducer.dataState
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

        changeInitial : (initial) => {
            dispatch(
                {
                    type: "ChangeInitial",
                    payload : initial
                }
            );
        },
        changeRole : (role) => {
            dispatch(
                {
                    type: "ChangeRole",
                    payload : role
                }
            );
        },
        changeDomain : (domain) => {
            dispatch(
                {
                    type: "ChangeDomain",
                    payload : domain
                }
            );
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dimensions);
