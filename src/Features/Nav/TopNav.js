import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const jQuery = require("jquery");

class TopNav extends React.Component {

    logout() {
        this.props.auth.logout();
    }

    collpaseMenu() {
        jQuery('body').toggleClass("sidebar-mini");
    }

    render() {

        return (

            <nav className="navbar navbar-transparent navbar-absolute">

                <div className="container-fluid">

                    {/*
                    <div className="navbar-minimize">
                        <button onClick={this.collpaseMenu.bind(this)} className="btn btn-round btn-white btn-fill btn-just-icon">
                            <i className="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i className="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    */}

                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand"> {this.props.initial}</a>
                    </div>


                    <div className="collapse navbar-collapse">
                        <ul className="nav navbar-nav navbar-right">

                            <li>
                                <Link to={'/dashboard/home'} className="dropdown-toggle text-center">
                                    <i className="material-icons">home</i>
                                    <p className="">Dashboard</p>
                                </Link>
                            </li>

                            <li>
                                <a className="dropdown-toggle text-center" data-toggle="dropdown" onClick={this.logout.bind(this)}>
                                    <i className="material-icons" title="Logout">power_settings_new</i>
                                    <p className=""></p>
                                </a>
                            </li>

                            <li className="separator hidden-lg hidden-md"></li>

                        </ul>
                    </div>


                </div>

            </nav>

        );
    }
}



const mapStateToProps = (state) => {
    return {
        initial: state.main.initial,
        role: state.main.role
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

        changeInitial: (initial) => {
            dispatch(
                {
                    type: "ChangeInitial",
                    payload: initial
                }
            );
        },
        changeRole: (role) => {
            dispatch(
                {
                    type: "ChangeRole",
                    payload: role
                }
            );
        }



    };
};


export default connect(mapStateToProps, mapDispatchToProps)(TopNav);
