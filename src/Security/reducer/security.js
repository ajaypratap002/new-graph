
const initialState = {
    products: [],
    user: {}
};

const securityReducer = (state = initialState, action) => {

    switch (action.type) {

        case "UpdateProducts":
            state = {
                ...state,
                products: action.payload,
            };
            break;

        case "UpdateUser":
            state = {
                ...state,
                user: action.payload,
            };
            break;

        default:

    }
    return state;

};


export default securityReducer;
