import React, { Component } from 'react';
import { connect } from 'react-redux';
import history from '../../Data/history';

class Validate extends Component {

  constructor(props) {
    super(props);

    this.state = {
      profile: []
    };

  }

  componentWillMount() {

    //Secure the page
    this.loginProcedure();


  }


  //Check if a user is logged in or not
  //Check if the user is a client or admin
  loginProcedure() {

    let userToken = localStorage.getItem('user');

    if (userToken) {

      console.log('user logged in');

    } else {
      console.log('here i m');
      this.props.history.push('/dashboard/login');
      this.setState({ profile: [] });
      console.log('User is not logged in');

    }

  }

  //Do the login action for non logged in users
  login() {
    // this.props.auth.login();
  }

  render() {

    return (

      <div></div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    role: state.main.role,
    api: state.main.api,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {

    changeInitial: (initial) => {
      dispatch(
        {
          type: "ChangeInitial",
          payload: initial
        }
      );
    },
    changeRole: (role) => {
      dispatch(
        {
          type: "ChangeRole",
          payload: role
        }
      );
    },
    changeClient: (role) => {
      dispatch(
        {
          type: "ChangeClient",
          payload: role
        }
      );
    },
    updateUserType: (type) => {
      dispatch(
        {
          type: "UpdateUserType",
          payload: type
        }
      );
    },
    updateProducts: (products) => {
      dispatch(
        {
          type: "UpdateProducts",
          payload: products
        }
      );
    },
    updateUser: (user) => {
      dispatch(
        {
          type: "UpdateUser",
          payload: user
        }
      );
    },
    changeSidebar: (sidebar) => {
      dispatch(
        {
          type: "ChangeSidebar",
          payload: sidebar
        }
      );
    },


  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Validate);
