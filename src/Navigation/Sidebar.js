import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import history from './../Data/history';
import swal from 'sweetalert';
const axios = require('axios');

class Sidebar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            clientChosen: '',
            userType: 'generic',

        };

    }


  logout() {

    let postURL = this.props.usersAPI + 'auth/api/logout/';

    axios.post(postURL).then((response) => {

      if (response.status === 200) {
        localStorage.removeItem("user");
        history.push('/dashboard/login');
      }

    }).catch((error) => {
      console.log(error);

      swal('Error', 'Error in logout!');

    });
  }

    render() {

        return (

            <div className="sidebar" data-active-color="red" data-background-color="white" data-image="">

                <div className="sidebar-wrapper">

                    <div className="collapse navbar-collapse">
                        <ul className="nav">

                        </ul>
                    </div>

                </div>

            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        initial: state.main.initial,
        role: state.main.role,
        api: state.main.api,
        client: state.main.client,
        userType: state.main.userType,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

        changeInitial: (initial) => {
            dispatch(
                {
                    type: "ChangeInitial",
                    payload: initial
                }
            );
        },
        changeRole: (role) => {
            dispatch(
                {
                    type: "ChangeRole",
                    payload: role
                }
            );
        },
        changeClient: (role) => {
            dispatch(
                {
                    type: "ChangeClient",
                    payload: role
                }
            );
        }
        ,
        changeClient: (role) => {
            dispatch(
                {
                    type: "ChangeClient",
                    payload: role
                }
            );
        },
        updateMultiChannelReportingStage: (setting) => {
            dispatch(
                {
                    type: "UpdateMultiChannelReportingStage",
                    payload: setting
                }
            );
        },
        updateMultiChannelReportingLive: (setting) => {
            dispatch(
                {
                    type: "UpdateMultiChannelReportingLive",
                    payload: setting
                }
            );
        }

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
