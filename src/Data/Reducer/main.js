let API = 'live api';


if (window.location.hostname === "localhost") {

    API = 'api goes here';


} else if (window.location.hostname === "192.168.2.85") {

    API = 'http://192.168.2.101:3000';

}

const initialState = {
    initial: 'Main',
    role: 'generic',
    api: API,

    domain: 'https://google.com',
    permissions: {
        super: {
            dashboard: true,
            campaigns: true,
        }
    },
    userType: 'editor',
    sidebar: 'fundraiser',
    userID: '',

    dashboard: 'donation',


    userAuth: "",
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case "ChangeUserAuth":
            state = {
                ...state,
                userAuth: action.payload,
            };
            break;


        case "ChangeInitial":
            state = {
                ...state,
                initial: action.payload,
            };
            break;

        case "ChangeRole":
            state = {
                ...state,
                role: action.payload,
            };
            break;
            
        case "ChangeSidebar":
            state = {
                ...state,
                sidebar: action.payload,
            };
            break;

        default:

    }
    return state;

};


export default reducer;
