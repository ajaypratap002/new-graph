import { combineReducers, createStore } from 'redux';

//Main App Reducer
import reducer from "../Reducer/main.js";


//User Management
import userReducer from "../../Views/Users/reducer/users.js";

const reducers = combineReducers({
  main: reducer,
  users: userReducer
});

export default createStore(reducers);
